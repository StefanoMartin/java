import java.awt.* ;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JFrame;
import java.util.*;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Dialog;

public class FracPanel implements ChangeListener, ActionListener{	
	JPanel highPanel, veryrightPanel, leftPanel;
	JPanel[] rightPanel;
	NewPanel np;
	JFrame f, f2;
	PicFractal myPic;
	JButton savePic;
	int 			s, times;
	double[] 		alpha, size, movx, movy;
	
	FracPanel(){
		np					= new NewPanel();
		this.myPic			= np.myPic;
		highPanel 			= new JPanel();
		veryrightPanel		= new JPanel(new GridLayout(np.s,6));
		rightPanel			= new JPanel[6];
		savePic				= new JButton("Click here to save a picture of your fractal");
		
		savePic.addActionListener(this);
		
		np.addNewSliderS(highPanel, np.sliderS);
		np.sliderS.js.addChangeListener(this);
		np.addNewSlider(highPanel, np.repetition);
		np.repetition.js.addChangeListener(this);

		for(int i = 0; i < np.s; i++){
			rightPanel[i]	= new JPanel(new GridLayout(6,3));
			np.addNewBigSlider(rightPanel[i], np.bigSlider[i]);
			np.bigSlider[i].gradeSlider.js.addChangeListener(this);
			np.bigSlider[i].sizeSlider.js.addChangeListener(this);
			np.bigSlider[i].XSlider.js.addChangeListener(this);
			np.bigSlider[i].YSlider.js.addChangeListener(this);
			veryrightPanel.add(rightPanel[i]);
		}
	}
	
	public void stateChanged(ChangeEvent e){
		s 			= np.sliderS.js.getValue();
		int oldS	= np.sliderS.oldValueSlider;
		
		if(s > oldS){
			for(int j = oldS; j < s; j++){
				rightPanel[j]	= new JPanel(new GridLayout(6,3));
				np.addNewBigSlider(rightPanel[j], np.bigSlider[j]);
				np.bigSlider[j].gradeSlider.js.addChangeListener(this);
				np.bigSlider[j].sizeSlider.js.addChangeListener(this);
				np.bigSlider[j].XSlider.js.addChangeListener(this);
				np.bigSlider[j].YSlider.js.addChangeListener(this);
				veryrightPanel.add(rightPanel[j]);
			}
			f.repaint();
		}else if(oldS > s){
			for(int j2 = s; j2 < oldS; j2++){
				veryrightPanel.remove(rightPanel[j2]);
			}
			f.repaint();
		}
		np.sliderS.oldValueSlider = s;
		
		//np.sliderS.sl.setText(s+","+oldS); //test
		
		times 	= np.repetition.js.getValue();
		int alphatemp = 0, sizetemp;
		alpha 		= new double[6];
		size 		= new double[6];
		movx		= new double[6];
		movy 		= new double[6];
		for(int i = 0; i < s; i++){
			alphatemp 	 = np.bigSlider[i].gradeSlider.js.getValue();
			alpha[i]	 = alphatemp*Math.PI*1.0/180;
			sizetemp 	 = np.bigSlider[i].sizeSlider.js.getValue();
			size[i]		 = sizetemp * 0.01;
			movx[i] 	 = np.bigSlider[i].XSlider.js.getValue()*1.0;
			movy[i] 	 = -np.bigSlider[i].YSlider.js.getValue()*1.0;
		}
		
		
		
		InitialPoint square = new InitialPoint();
		Fractal myFractal = new Fractal(square);
		for(int j = 0; j < times; j++)
			myFractal = new Fractal(myFractal, alpha, size, movx, movy, s);
		
		f2.remove(myPic);
		myPic = new PicFractal(myFractal);
		f2.add(myPic);
		f2.repaint();
		f2.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		BufferedImage image=new BufferedImage(myPic.getWidth(), myPic.getHeight(),BufferedImage.TYPE_4BYTE_ABGR_PRE);
		Graphics2D g2=(Graphics2D)image.getGraphics();
		//g2.setPaint ( new Color ( 0, 0, 0 ) );
		//g2.fillRect ( 0, 0, image.getWidth(), image.getHeight() );
		//myPic.setPaint( Color.RED );
		g2.setPaint( new Color(255,0,0));
		myPic.paint(g2);
		String nameimage = "S"+s+"T"+times;
		for(int j = 0; j < s; j++){
			int alphatemp	= (int)(alpha[j]*180/Math.PI);
			int sizetemp 	= (int)(size[j]*100);
			int movxtemp 	= (int)(movx[j]);
			int movytemp 	= (int)(-movy[j]);
			nameimage += "S"+(j+1)+"times"+times+"alpha"+alphatemp+"size"+sizetemp+"movx"+movxtemp+"movy"+movytemp;
		}
		
		nameimage += ".png";
		try {
			ImageIO.write(image, "png", new File(nameimage));
			JFrame littleframe = new JFrame();
			JOptionPane.showMessageDialog(littleframe, "Your picture was saved in\n"+nameimage);
		} catch (Exception e2) {}
	}
	
	
	public static void main(String args[]){
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim 	= toolkit.getScreenSize();
		FracPanel fp 	= new FracPanel();
		fp.f 	 		= new JFrame();
		fp.f2	 		= new JFrame();
		fp.f.setTitle("Fractals");
		fp.f.add(fp.highPanel, BorderLayout.NORTH);
		fp.f.add(fp.veryrightPanel, BorderLayout.WEST);
		fp.f.add(fp.savePic, BorderLayout.SOUTH);
		fp.f.setSize(dim.width/2,dim.height);  
		fp.f.setVisible(true);
		fp.f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE ); 
		fp.f2.add(fp.myPic);
		fp.f2.setLocation(dim.width/2,0);
		fp.f2.setSize(dim.width/2,dim.height);  
		fp.f2.setVisible(true);
		fp.f2.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE ); 
	}
		
		

}