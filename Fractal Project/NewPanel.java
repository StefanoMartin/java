import javax.swing.JPanel;

public class NewPanel{	
	SliderS 		sliderS;
	NewSlider 		repetition;
	NewBigSlider[] 	bigSlider;
	int 			s, times;
	double[] 		alpha, size, movx, movy;
	Fractal 		myFractal;
	PicFractal  	myPic;
		
	NewPanel(){
		sliderS 	= new SliderS();
		repetition 	= new NewSlider("Times",1,10,4,1);
		bigSlider 	= new NewBigSlider[6];
		
		int alphatemp, sizetemp;
		s 			= sliderS.valueSlider;
		times 		= repetition.valueSlider;
		alpha 		= new double[6];
		size 		= new double[6];
		movx		= new double[6];
		movy 		= new double[6];
		for(int i = 0; i < 6; i++){
			bigSlider[i] = new NewBigSlider(i);
			alphatemp 	 = bigSlider[i].gradeSlider.valueSlider;
			alpha[i]	 = alphatemp*Math.PI*1.0/180;
			sizetemp 	 = bigSlider[i].sizeSlider.valueSlider;
			size[i]		 = sizetemp * 0.01;
			movx[i] 	 = -bigSlider[i].XSlider.valueSlider*1.0;
			movy[i] 	 = bigSlider[i].YSlider.valueSlider*1.0;
		}
		
		InitialPoint square = new InitialPoint();
		myFractal	= new Fractal(square);
		for(int j = 0; j < times; j++)
			myFractal = new Fractal(myFractal, alpha, size, movx, movy, s);
		myPic 				= new PicFractal(myFractal);
	}
	
	public void addNewSliderS(JPanel jp, SliderS ns){
		JPanel jp2 = new JPanel();
		jp2.add(ns.sl);
		jp2.add(ns.js);
		jp2.add(ns.tf);
		jp.add(jp2);
	}
	
	public void addNewSlider(JPanel jp, NewSlider ns){
		JPanel jp2 = new JPanel();
		jp2.add(ns.sl);
		jp2.add(ns.js);
		jp2.add(ns.tf);
		jp.add(jp2);
	}
	
	public void addNewBigSlider(JPanel jp, NewBigSlider nbs){
		jp.add(nbs.nameS);
		addNewSlider(jp, nbs.gradeSlider);
		addNewSlider(jp, nbs.sizeSlider);
		addNewSlider(jp, nbs.XSlider);
		addNewSlider(jp, nbs.YSlider);
	}
}