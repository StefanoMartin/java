import java.awt.* ;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

public class SliderS implements ChangeListener{
	int 	valueSlider, oldValueSlider;
	JLabel 	sl, tf;
	JSlider js;

	SliderS(){ 
		sl = new JLabel("S", JLabel.CENTER);
		js = new JSlider(JSlider.HORIZONTAL, 1, 6, 2);
		tf = new JLabel("2", JLabel.CENTER);
		valueSlider 	= 2;
		oldValueSlider	= 2;
		
		js.setMinorTickSpacing(1);
		Font font = new Font("Serif", Font.ITALIC, 15);
		js.setFont(font);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		for(int grade = 1; grade <= 6; grade = grade + 1)
			labelTable.put(new Integer(grade), new JLabel(""+grade));
		js.setLabelTable( labelTable );
		js.setPaintLabels(true);
		js.setValue(2);
		js.addChangeListener(this);
	
		Font font2 = new Font("Serif", Font.ITALIC, 30);
		tf.setFont(font2);
	}
	
	public void stateChanged(ChangeEvent e){
		valueSlider = js.getValue();
		tf.setText(valueSlider+"");
		oldValueSlider = valueSlider;  
	}
}