public class Fractal{
	Point[] pointFractals;
	
	Fractal(InitialPoint square){
		pointFractals = square.squarepoints;
	}
	
	Fractal(Fractal prevFrac, double[] alpha, double[] size, double[] movx, double[] movy, int s){
		int numberPointsPrevFrac = prevFrac.pointFractals.length;
		int numberPoints = (s+1)*numberPointsPrevFrac;
		int oldx, oldy, newx, newy; 
		this.pointFractals = new Point[numberPoints];
		
		for(int i = 0; i < numberPointsPrevFrac; i++){
			Point a = prevFrac.pointFractals[i];
			this.pointFractals[i] = a;
			oldx = a.x;
			oldy = a.y;
			for(int j = 0; j < s; j++){
				newx = (int)(size[j]* (Math.cos(alpha[j])*oldx - Math.sin(alpha[j])*oldy) + movx[j]);
				newy = (int)(size[j]* (Math.sin(alpha[j])*oldx + Math.cos(alpha[j])*oldy) + movy[j]);
				this.pointFractals[i + numberPointsPrevFrac*(j+1)] = new Point(newx, newy);
			}
		}
	}
}

class Point{
	int x;
	int y;
	Point(int x, int y){
		this.x = (int)x;
		this.y = (int)y;
	}
}

class InitialPoint{
	Point point1 = new Point(0,0);
	Point point2 = new Point(0,100);
	Point point3 = new Point(100,100);
	Point point4 = new Point(100,0);
	Point[] squarepoints = {point1, point2, point3, point4};
}