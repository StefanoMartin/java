import java.awt.*; // For Font
import javax.swing.*; // For JLabel and JSlider
import javax.swing.event.*; // For ChangeListener
import java.util.*; // For Hashtable

public class NewSlider implements ChangeListener{
	int valueSlider;
	JLabel sl, tf;
	JSlider js;

	NewSlider(String name, int start, int arrive, int defaultSlider, int lable){
		sl = new JLabel(name, JLabel.CENTER);
		js = new JSlider(JSlider.HORIZONTAL, start, arrive, defaultSlider);
		tf = new JLabel(""+defaultSlider, JLabel.CENTER);
		valueSlider = defaultSlider;
	
		js.setMinorTickSpacing(1);
		Font font = new Font("Serif", Font.ITALIC, 15);
		js.setFont(font);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		for(int grade = start; grade <= arrive; grade = grade + lable)
			labelTable.put( new Integer( grade ), new JLabel(""+grade ));
		js.setLabelTable( labelTable );
		js.setPaintLabels(true);
		js.setValue(defaultSlider);
		js.addChangeListener(this);
		
		Font font2 = new Font("Serif", Font.ITALIC, 30);
		tf.setFont(font2);
	}
	
	public void stateChanged(ChangeEvent e){
		valueSlider = js.getValue();
		tf.setText(valueSlider+"");
	}
}

class NewBigSlider{
	NewSlider gradeSlider;
	NewSlider sizeSlider;
	NewSlider XSlider;
	NewSlider YSlider;
	JLabel nameS;
	
	NewBigSlider(int i){
		Font font 		= new Font("Serif", Font.ITALIC, 30);
		nameS 	 	 	= new JLabel("S"+(i+1), JLabel.CENTER);
		nameS.setFont(font);
		gradeSlider 	= new NewSlider("Grade",-180,180,45,90);
		sizeSlider		= new NewSlider("Size",0,100,50,25);
		XSlider 		= new NewSlider("X axis",-500,500,100,200);
		YSlider 		= new NewSlider("Y axis",-500,500,100,200);	
	}
}