import java.awt.*;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.Toolkit;

public class PicFractal extends Canvas{
	Fractal frac;
	int numberPointsFrac;
	int numberSquares;
	int x1, x2, y1, y2;
	
	public void paint(Graphics g){
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim 	= toolkit.getScreenSize();
		int xw = dim.width/4;
		int yh = dim.height/2;
		for(int i=0; i<numberSquares; i++){
			for(int j=0; j<3; j++){
				x1 = this.frac.pointFractals[4*i+j].x+xw;
				y1 = this.frac.pointFractals[4*i+j].y+yh;
				x2 = this.frac.pointFractals[4*i+j+1].x+xw;
				y2 = this.frac.pointFractals[4*i+j+1].y+yh;
				g.drawLine(x1,y1,x2,y2);
			}
			x1 = this.frac.pointFractals[4*i+3].x+xw;
			y1 = this.frac.pointFractals[4*i+3].y+yh;
			x2 = this.frac.pointFractals[4*i].x+xw;
			y2 = this.frac.pointFractals[4*i].y+yh;
			g.drawLine(x1,y1,x2,y2);
		}
		setForeground(Color.RED);
		setBackground(Color.WHITE);
	}
	
	PicFractal(Fractal frac){
		this.frac = frac;
		this.numberPointsFrac = frac.pointFractals.length;
		this.numberSquares = (int)numberPointsFrac/4;
	}
}
	