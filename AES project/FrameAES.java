import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;  
import java.awt.Toolkit;

public class FrameAES extends JFrame implements ActionListener{ // User Friendly Interface.
	JFrame fmain, fsfiles;
	JLabel lsf, lo, lp, lb, lk;
	JRadioButton b1, b2, b3, k1, k2, k3, e1, e2;
	JTextField tf;
	JPasswordField pf;
	ButtonGroup bgb, bgk, bge;
	JButton buttonAES, buttonSelectFiles;
	File selectedFile;
	ButtonSelectFiles bsf;
	Encoding aes;

	FrameAES(){
		bsf = new ButtonSelectFiles();
		buttonSelectFiles = bsf.buttonSelectFiles;
		lsf = bsf.lsf;
		add(buttonSelectFiles); add(lsf);
		
		lo = new JLabel("Name of output:");
		lo.setBounds(20, 60, 130, 25);
		tf = new JTextField();
		tf.setText("EncryptedFile.txt");
		tf.setBounds(160, 60, 200, 25);
		add(lo); add(tf);
		
		lp = new JLabel("Password:");
		lp.setBounds(20, 100, 130, 25);
		pf = new JPasswordField();
		pf.setBounds(160, 100, 200, 25);
		add(lp); add(pf);
		
		lb 	= new JLabel("Security:");
		lb.setBounds(20, 140, 70, 25); 
		b1 = new JRadioButton("AES-128"); 		b1.setBounds(110,140,90,25);
		b2 = new JRadioButton("AES-192"); 		b2.setBounds(200,140,90,25);
		b3 = new JRadioButton("AES-256", true); b3.setBounds(290,140,90,25);
		bgb = new ButtonGroup();
		bgb.add(b1); bgb.add(b2); bgb.add(b3);
		add(lb); add(b1); add(b2); add(b3);
		
		lk 	= new JLabel("Size key:");
		lk.setBounds(20, 180, 200, 25); 
		k1 = new JRadioButton("4"); 		k1.setBounds(110,180,50,25);
		k2 = new JRadioButton("6"); 		k2.setBounds(200,180,50,25);
		k3 = new JRadioButton("8", true); 	k3.setBounds(290,180,50,25);
		bgk = new ButtonGroup();
		bgk.add(k1); bgk.add(k2); bgk.add(k3);
		add(lk); add(k1); add(k2); add(k3);
		
		e1 = new JRadioButton("Encoding", true); 	e1.setBounds(90,220,90,25);
		e2 = new JRadioButton("Decoding"); 			e2.setBounds(220,220,90,25);
		bge = new ButtonGroup();
		bge.add(e1); bge.add(e2);
		add(e1); add(e2);
		
		buttonAES = new JButton("Use AES");	buttonAES.setBounds(150, 260, 90, 30);
		buttonAES.addActionListener(this);
		add(buttonAES);
		
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim 	= toolkit.getScreenSize();
		
		setTitle("Encryption - Decryption AES");
		setSize(400,360);
		setLocation(dim.width/2 - 200, 100);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}
	
	public void actionPerformed(ActionEvent e){
		try{ 
			String s = "Your file "+bsf.selectedFile.getName();
			char[] passwordchar = pf.getPassword();
			if(passwordchar.length == 0){
				JFrame littleframe = new JFrame();
				JOptionPane.showMessageDialog(littleframe, "Insert a password.");
			}else{
				String encodedFile = tf.getText();
				if(encodedFile.length() == 0){
					JFrame littleframe = new JFrame();
					JOptionPane.showMessageDialog(littleframe, "Select a name for your input.");
				}else{
					String password = new String(passwordchar);
					
					boolean enc = true;
					if(e2.isSelected()){
						enc = false;
						s += " has been decrypted ";
					}
					else
						s += " has been encrypted ";
					
					s += "in "+ encodedFile + "\nwith AES-";
					
					int n = 256;
					if(b1.isSelected())
						n = 128;
					else if(b2.isSelected())
						n = 192;
					
					s += n;
				
					int nk = 8;
					if(k1.isSelected())
						nk = 4;
					else if(k2.isSelected())
						nk = 6;
				
					s += " and key large "+nk+".";
				
					aes = new Encoding(bsf.selectedFile, encodedFile, password, n, nk, enc);
					
					JFrame littleframe = new JFrame();
					JOptionPane.showMessageDialog(littleframe, s);
				}
			}
		}catch(Exception e2){
			JFrame littleframe = new JFrame();
			JOptionPane.showMessageDialog(littleframe, "Select a file.");
			System.out.println(e2);
		}
	}
	
	public static void main(String[] args){
		new FrameAES();
	}
}