public class OperationsInverseAES{ // Operations necessary for Decoding
	GF256 f256 = new GF256();
	
	public int SBoxInverse(int Aij){ // Inverse of the SBox
		int[] x = new int[8];
		int[] y = new int[8];
		for(int i=0; i<8; i++){
			x[i] = Aij % 2;
			Aij = (Aij - x[i])/2;
		}
		x[0] = x[0] + 1;
		x[1] = x[1] + 1;
		x[5] = x[5] + 1;
		x[6] = x[6] + 1;
		y[0] = (x[2] + x[5] + x[7] + 256) % 2;
		y[1] = (x[3] + x[6] + x[0] + 256) % 2;
		y[2] = (x[4] + x[7] + x[1] + 256) % 2;
		y[3] = (x[5] + x[0] + x[2] + 256) % 2;
		y[4] = (x[6] + x[1] + x[3] + 256) % 2;
		y[5] = (x[7] + x[2] + x[4] + 256) % 2;
		y[6] = (x[0] + x[3] + x[5] + 256) % 2;
		y[7] = (x[1] + x[4] + x[6] + 256) % 2;
		int a = 1;
		Aij = 0;
		for(int j=0; j<8; j++){
			Aij = Aij + y[j]*a;
			a	= 2*a;
		}
		Aij = f256.inversionGF256(Aij);
		return Aij;
	}
	
	public int[][] ByteSubInverse(int[][] A, int nb){ // Application of the inverse of SBox
		for(int j = 0; j < nb; j++){
			for(int i = 0; i < 4; i++){
				A[i][j] = SBoxInverse(A[i][j]);
			}
		}
		return A;
	}
	
	public int[] ShiftRowOfSomethingInverse(int[] Arow, int n){ 
		int k = Arow.length;
		int[] Brow = new int[k];
		for(int i = 0; i < n; i++)
			Brow[i] = Arow[k-n+i] ;
		for(int j = 0; j < k-n; j++)
			Brow[n+j] = Arow[j];
		return Brow;
	}
	
	public int[][] ShiftRowsInverse(int[][] A, int Nb){ // Inverse of MixingLayer
		A[1] = ShiftRowOfSomethingInverse(A[1], 1);
		if(Nb != 8){
			A[2] = ShiftRowOfSomethingInverse(A[2], 2);
			A[3] = ShiftRowOfSomethingInverse(A[3], 3);
		}
		else{
			A[2] = ShiftRowOfSomethingInverse(A[2], 3);
			A[3] = ShiftRowOfSomethingInverse(A[3], 4);
		}
		return A;
	}
	
	public int[][] MixColumnInverse (int[][] A, int Nb){ // Inverse of MixColumn
		int b0, b1, b2, b3;
		PolyGF256 a;
		PolyGF256 b = new PolyGF256(14,9,13,11);
		PolyGF256 d;
		for(int j = 0; j<Nb; j++){
			a = new PolyGF256(A[0][j],A[1][j],A[2][j],A[3][j]);
			d = f256.polyMult(b, a);

			A[0][j] = d.a0;
			A[1][j] = d.a1;
			A[2][j] = d.a2;
			A[3][j] = d.a3;
		}
		return A;
	}
	
	public static void main(String args[]){
		OperationsInverseAES o = new OperationsInverseAES();
		System.out.println(o.SBoxInverse(1));
		System.out.println(o.SBoxInverse(0));
	}
}
	