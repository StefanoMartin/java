public class PolyGF256{ // Polynomial of degree 4 in GF(256) modulus x^4 + 1
	int a0, a1, a2, a3;
	GF256 f = new GF256();
	
	PolyGF256(int a0, int a1, int a2, int a3){
		this.a0 = a0;
		this.a1 = a1;
		this.a2 = a2;
		this.a3 = a3;
	}
}