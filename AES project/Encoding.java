import java.util.Scanner;
import java.io.*;

public class Encoding{ // Program to Encode and Decode
	String nameFile;
	String encodedFile;
	int sizeState;
	int sizeKey;
	int numberRound;
	int numberColumns;
	int numberRows = 4;
	OperationsEncoding oE;
	boolean encryption;
	
	
	String AESEncodingBytes(int[] bytes, KeySchedule kS){  // Methods to encode/decode bytes
		int[][] state = new int[numberRows][numberColumns]; // Here I create my State
		for(int column = 0; column < numberColumns; column++){
			for(int row = 0; row < numberRows; row++){
				if(encryption)
					state[row][column] = bytes[numberRows*column+row];
				else
					state[row][column] = bytes[numberRows*column+row] - 32; // I subtract 32 because in the encoding I added 32
			}
		}
		
		if(encryption)
			state = oE.encryptRijindael(state, kS); // Encryption
		else // decryption
			state = oE.decryptRijindael(state, kS); // Decryption
		
		int tempchar;
		String s = "";
		for(int column2 = 0; column2 < numberColumns; column2++){
			if(encryption){
				for(int row2 = 0; row2 < numberRows; row2++){
					tempchar = state[row2][column2] + 32; // I sum 32 to avoid overlapping with the first 32 character of Unicode 
					s += Character.toString((char)tempchar); // Note all tempchar are integers between 32 and 255+32
				}
			}else{ // decryption
				for(int row2 = 0; row2 < numberRows/2; row2++){
					tempchar = state[2*row2][column2]*256 + state[2*row2+1][column2]; // I recover the original unicode elements
					if(tempchar != 0){
						s += Character.toString((char)tempchar);
					}
				}
			}
		}

		return s;
	}
	
	boolean isBytesNo0(int[] bytes){ 
		int a = 0;
		for(int i = 0; i < bytes.length; i++)
			a += bytes[i];
		if(a != 0)
			return true;
		else
			return false;
	}
	
	
	String AESEncoding(String s, KeySchedule kS){ // I encrypt / decrypt a String
		String encodedS = "";
		int i = 0;
		int temp;
		int[] bytes = new int[4*numberColumns];
		String check;
		
		while(i < s.length()){
			bytes = new int[4*numberColumns];
			if(encryption){	// encryption
				for(int j = 0; j < 2*numberColumns; j++){
					temp = 0;  // I consider any encryption unicode element as a number between 0 and 256^2-1. 
					if(2*i*numberColumns + j < s.length()){
						temp = Character.codePointAt(s, i*2*numberColumns + j);
					}
					bytes[2*j+1] = temp % 256;
					temp = (temp - bytes[2*j+1])/256;
					bytes[2*j] = temp;  
				}
			}else{ // decryption
				for(int j = 0; j < 4*numberColumns; j++){ // I consider any decrpytion unicode element as a number between 0 and 255. 
					if(4*i*numberColumns + j < s.length()-1) // I don't watch the last char because it is \n and it was added by me.
						bytes[j] = Character.codePointAt(s, 4*i*numberColumns + j);
				}
			}
			
			if(isBytesNo0(bytes)) // I don't want to encrypt the spaces
				encodedS += AESEncodingBytes(bytes, kS);
			i++;
		}
		if(encryption)
			return encodedS + "\n"; // I add \n to make the next decryption faster
		else
			return encodedS;
	}
	
	Encoding(File file, String encodedFile, String password, int sizeState, int sizeKey, boolean encryption){
        try{
			this.nameFile 		= file.getName();
			this.encodedFile	= encodedFile;
			this.sizeState 		= sizeState;
			this.sizeKey		= sizeKey;
			this.encryption 	= encryption;
			numberColumns 		= sizeState / 32;
			
			if(numberColumns == 4 & sizeKey == 4)
				numberRound = 10;
			else if((numberColumns == 4 & sizeKey == 6) || (numberColumns == 6 && (sizeKey == 4 || sizeKey == 6)))
				numberRound = 12;
			else
				numberRound = 14;
			
			oE 					= new OperationsEncoding(numberColumns, numberRound);
			KeySchedule kS 		= new KeySchedule(password, numberColumns, sizeKey, numberRound);
			
            String str;
			FileInputStream 	 fileInput 	  = new FileInputStream(file);
			InputStreamReader 	 readerInput  = new InputStreamReader(fileInput, "UTF-8");
			BufferedReader       buffReader   = new BufferedReader(readerInput);
			FileOutputStream 	 fileOutput   = new FileOutputStream(encodedFile);
			OutputStreamWriter   writerOutput = new OutputStreamWriter(fileOutput, "UTF-8");
			BufferedWriter       buffWriter   = new BufferedWriter(writerOutput);
			
			String encodedS;
			String s, temp;

			while((s = buffReader.readLine()) != null)
            {
                temp = s + "\n";
				encodedS = AESEncoding(temp, kS);
				buffWriter.write(encodedS);
            }
			
			buffWriter.close();
			buffReader.close();
        }
        catch(Exception e)
        {
			System.out.println("File not found");
			System.out.println(e);
			System.exit(1);
        }
    }
}