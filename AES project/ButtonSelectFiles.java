import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JFileChooser;
import java.io.File;  

public class ButtonSelectFiles implements ActionListener{ // Java file to choose files.
	JLabel lsf;
	JButton buttonSelectFiles;
	File selectedFile;
	
	ButtonSelectFiles(){
		buttonSelectFiles = new JButton("Select File");	
		buttonSelectFiles.setBounds(20, 20, 110, 30);
		buttonSelectFiles.addActionListener(this);
		lsf = new JLabel();
		lsf.setBounds(160, 25, 200, 25);
	}
	
	public void actionPerformed(ActionEvent e){
		JFrame fsfiles = new JFrame();
		JFileChooser fc = new JFileChooser();
		fsfiles.setLayout(null);
		fsfiles.setVisible(false);
		int result = fc.showOpenDialog(fsfiles);
		if (result == JFileChooser.APPROVE_OPTION){
			selectedFile = fc.getSelectedFile();
			lsf.setText(selectedFile.getName());
		}
		fsfiles.dispose();
	}
}