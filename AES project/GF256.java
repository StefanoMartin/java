public class GF256{	 // I define the field with 256 elements
	int[] xpoly		= new int[15];
	int[] polygen 	= new int[7];
	Conversion256 conv = new Conversion256();
	
	GF256(){
		polygen[0]	= 0b00011011;	// (x^4+x^3+x+1)*x^0 mod (x^8+x^4+x^3+x+1) 
		polygen[1]	= 0b00110110;
		polygen[2]	= 0b01101100;
		polygen[3]	= 0b11011000;  // (x^4+x^3+x+1)*x^3 mod (x^8+x^4+x^3+x+1) 
		polygen[4]	= 0b10101011;
		polygen[5]	= 0b01001101;
		polygen[6]	= 0b10011010;
		for(int i=0; i<15; i++)
			xpoly[i] = 0x1 << i;
	}
	
	public int multiplicationByX(int a){ // Multiplication by X
			a 		= a << 1;
		if((xpoly[0] & a) != 0b0){
			a = a ^ polygen[0];
			a = a ^ xpoly[0];
		}
		return a;
	}
	
	public int multiplicationGF256(int a, int b){ // Multiplication of two elements in GF(256)
		if(a == 0 || b == 0)
			return 0;
		else{
			int c = (conv.P[a] + conv.P[b]) % 255; // We look to the power structure of the element
			return conv.M[c];
		}
	}
	
	public int inversionGF256(int a){ // Inversion of an element in GF(256)
		if(a == 0)
			return 0;
		else{
			int c = (255 - conv.P[a]) % 255;
			return conv.M[c];
		}
	}
	
	public PolyGF256 polyMult(PolyGF256 a, PolyGF256 b){ // Multiplication between two polynomials in GF(256)[x] mod (x^4 + 1)
		int d0, d1, d2, d3;
		
		d0 = multiplicationGF256(a.a0, b.a0)^multiplicationGF256(a.a3, b.a1)^multiplicationGF256(a.a2, b.a2)^multiplicationGF256(a.a1, b.a3);
		d1 = multiplicationGF256(a.a1, b.a0)^multiplicationGF256(a.a0, b.a1)^multiplicationGF256(a.a3, b.a2)^multiplicationGF256(a.a2, b.a3);
		d2 = multiplicationGF256(a.a2, b.a0)^multiplicationGF256(a.a1, b.a1)^multiplicationGF256(a.a0, b.a2)^multiplicationGF256(a.a3, b.a3);
		d3 = multiplicationGF256(a.a3, b.a0)^multiplicationGF256(a.a2, b.a1)^multiplicationGF256(a.a1, b.a2)^multiplicationGF256(a.a0, b.a3);
		
		PolyGF256 d = new PolyGF256(d0,d1,d2,d3);
		
		return d;
	}
	
	public PolyGF256 polyMultByX(PolyGF256 a){ // Multiplication by x with a polynomial in GF(256)[x] mod (x^4 + 1)
		PolyGF256 d = new PolyGF256(a.a3, a.a0, a.a1,a .a2);
		return d;
	}
	

	
	public static void main(String args[]){
		GF256 f256 = new GF256();
		int a = 0b00111011;
		int b = 0b00101001;
		System.out.println(Integer.toBinaryString(f256.xpoly[12]));
		System.out.println(Integer.toBinaryString(a));
		System.out.println(Integer.toBinaryString(b));
		System.out.println(Integer.toBinaryString(a^b));
		int c = f256.multiplicationByX(a);
		System.out.println(Integer.toBinaryString(c));
	}	
}

