public class OperationsEncoding{
	OperationsAES oAES = new OperationsAES();
	OperationsInverseAES oiAES = new OperationsInverseAES();
	int nb, nr;
	
	OperationsEncoding(int nb, int nr){
		this.nb = nb;
		this.nr = nr;
	}
	
	public int[][] encryptionRound(int[][] B, int[][] RoundKey){
		B = oAES.ByteSub(B, nb); 
		B = oAES.ShiftRows(B, nb); 
		B = oAES.MixColumn(B, nb); 
		B = oAES.AddRoundKey(B, RoundKey, nb);
		return B;
	}
	
	public int[][] encryptionFinalRound(int[][] B, int[][] RoundKey){
		B = oAES.ByteSub(B, nb); 
		B = oAES.ShiftRows(B, nb); 
		B = oAES.AddRoundKey(B, RoundKey, nb);
		return B;
	}
	
	public int[][] decryptionFirstRound(int[][] B, int[][] RoundKey){
		B = oAES.AddRoundKey(B, RoundKey, nb);
		B = oiAES.ShiftRowsInverse(B, nb);
		B = oiAES.ByteSubInverse(B, nb);
		return B;
	}
	
	public int[][] decryptionRound(int[][] B, int[][] RoundKey){
		B = oAES.AddRoundKey(B, RoundKey, nb);
		B = oiAES.MixColumnInverse(B, nb);
		B = oiAES.ShiftRowsInverse(B, nb);
		B = oiAES.ByteSubInverse(B, nb);
		return B;
	}
	
	public int[][] encryptRijindael(int[][] B, KeySchedule W){
		B = oAES.AddRoundKey(B, W.RK[0], nb);
		for(int i = 1; i < nr; i++)
			B = encryptionRound(B, W.RK[i]);
		B = encryptionFinalRound(B, W.RK[nr]);
		return B;
	}
	
	public int[][] decryptRijindael(int[][] B, KeySchedule W){
		B = decryptionFirstRound(B, W.RK[nr]);
		for(int i = nr-1; i > 0; i--)
			B = decryptionRound(B, W.RK[i]);
		B = oAES.AddRoundKey(B, W.RK[0], nb);
		return B;
	}
}