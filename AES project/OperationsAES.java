public class OperationsAES{ // Operations necessary for Encoding
	GF256 f256 = new GF256();
	
	public int SBox(int Aij){ // SBox of AES
		Aij = f256.inversionGF256(Aij);
		//System.out.print(Aij + " , ");
		int[] x = new int[8];
		int[] y = new int[8];
		for(int i=0; i<8; i++){
			x[i] = Aij % 2;
			Aij = (Aij - x[i])/2;
		}
		y[0] = (x[0] + 					   x[4] + x[5] + x[6] + x[7] + 1) % 2;
		y[1] = (x[0] + x[1] + 					  x[5] + x[6] + x[7] + 1) % 2;
		y[2] = (x[0] + x[1] + x[2] + 				   + x[6] + x[7]    ) % 2;
		y[3] = (x[0] + x[1] + x[2] + x[3] +						x[7]    ) % 2;
		y[4] = (x[0] + x[1] + x[2] + x[3] + x[4]  					    ) % 2;
		y[5] = (   	   x[1] + x[2] + x[3] + x[4] + x[5] +			   1) % 2;
		y[6] = (			  x[2] + x[3] + x[4] + x[5] + x[6] +       1) % 2;
		y[7] = (					 x[3] + x[4] + x[5] + x[6] + x[7]   ) % 2;
		int a = 1;
		for(int j=0; j<8; j++){
			Aij = Aij + y[j]*a;
			a	= 2*a;
		}
		return Aij;
	}
	
	public int[][] ByteSub(int[][] A, int nb){ // Application of SBox
		for(int j = 0; j < nb; j++){
			for(int i = 0; i < 4; i++){
				A[i][j] = SBox(A[i][j]);
			}
		}
		return A;
	}
	
	public int[] ShiftRowOfSomething(int[] Arow, int n){ 
		int k = Arow.length;
		int[] Brow = new int[k];
		for(int i = 0; i < n; i++)
			Brow[k-n+i] = Arow[i];
		for(int j = 0; j < k-n; j++)
			Brow[j] = Arow[n+j];
		return Brow;
	}
	
	public int[][] ShiftRows(int[][] A, int Nb){ // MixingLayer
		A[1] = ShiftRowOfSomething(A[1], 1);
		if(Nb != 8){
			A[2] = ShiftRowOfSomething(A[2], 2);
			A[3] = ShiftRowOfSomething(A[3], 3);
		}
		else{
			A[2] = ShiftRowOfSomething(A[2], 3);
			A[3] = ShiftRowOfSomething(A[3], 4);
		}
		return A;
	}
	
	public int[][] MixColumn(int[][] A, int Nb){ // Mix Column
		//int b0, b1, b2, b3;
		PolyGF256 a;
		PolyGF256 b = new PolyGF256(2,1,1,3);
		PolyGF256 d;
		for(int j = 0; j<Nb; j++){
			a = new PolyGF256(A[0][j],A[1][j],A[2][j],A[3][j]);
			d = f256.polyMult(b, a);
			
			A[0][j] = d.a0;
			A[1][j] = d.a1;
			A[2][j] = d.a2;
			A[3][j] = d.a3;
		}
		return A;
	}
	
	public int[][] AddRoundKey(int[][] A, int[][] K, int Nb){ // Add key <-- This necessary for Decoding too
		int[][] B = new int[4][Nb];
		for(int j = 0; j < Nb; j++){
			for(int i = 0; i < 4; i++)
				B[i][j] = A[i][j] ^ K[i][j];
		}
		return B;
	}
	
	public static void main(String args[]){
		OperationsAES o = new OperationsAES();
		System.out.println(o.SBox(1));
		System.out.println(o.SBox(0));
	}
}
	