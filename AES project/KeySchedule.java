public class KeySchedule{ // This is the KeySchedule of AES
	GF256 f256 = new GF256();
	int nb;      	// Number column of a state
	int nc = 4;  	// Number row of a state
	int nk;			// Length of our key
	int nr;			// Number rounds
	int[][] W;		// Extended Key
	int[][][] RK;	// Round Keys
	
	KeySchedule(String s, int nb, int nk, int nr){
		this.nb = nb;
		this.nk = nk;
		this.nr = nr;
		int k = s.length();	
		//int[] bytes = new int[k];
		int[] Key = new int[4*nk];
		
		for(int j = 0; j < 4*nk; j++){
			if(j < k){
				Key[j] = Character.codePointAt(s, j) % 256;
				//Key[j] = (bytes[j] + 256) % 256;
			}
		}
		
		W = KeyExpansion(Key, nb, nr, nk);
		RK = CreationRoundKeys(W, nr, nb);
	}
	
	public String toString(){
		String toPrint = "";
		for(int i = 0; i < nb*(nr+1); i++){
			for(int j = 0; j < 4; j++){
					toPrint += W[i][j]+", ";
			}
			toPrint += "\n";
		}
		return toPrint;
	}
	
	public int SBox(int Aij){
		Aij = f256.inversionGF256(Aij);
		int[] x = new int[8];
		int[] y = new int[8];
		for(int i=0; i<8; i++){
			x[i] = Aij % 2;
			Aij = (Aij - x[i])/2;
		}
		y[0] = (x[0] + 					   x[4] + x[5] + x[6] + x[7] + 1) % 2;
		y[1] = (x[0] + x[1] + 					  x[5] + x[6] + x[7] + 1) % 2;
		y[2] = (x[0] + x[1] + x[2] + 				   + x[6] + x[7]    ) % 2;
		y[3] = (x[0] + x[1] + x[2] + x[3] +						x[7]    ) % 2;
		y[4] = (x[0] + x[1] + x[2] + x[3] + x[4]  					    ) % 2;
		y[5] = (   	   x[1] + x[2] + x[3] + x[4] + x[5] +			   1) % 2;
		y[6] = (			  x[2] + x[3] + x[4] + x[5] + x[6] +       1) % 2;
		y[7] = (					 x[3] + x[4] + x[5] + x[6] + x[7]   ) % 2;
		int a = 1;
		for(int j=0; j<8; j++){
			Aij = Aij + y[j]*a;
			a	= 2*a;
		}
		return Aij;
	}
	
	public int[] SubByte(int[] W){
		int[] V = {SBox(W[0]), SBox(W[1]), SBox(W[2]), SBox(W[3])};
		return V;
	}
	
	public int[] RotByte(int[] W){
		int[] V = {W[1], W[2], W[3], W[0]};
		return V;
	}
	
	public int[] CreationRcon(int Nb, int Nr, int Nk){
		int[] R = new int[Nb*(Nr+1)/Nk + 1];
		R[0] = 1;
		for(int i = 1 ; i < Nb*(Nr+1)/Nk + 1; i++)
			R[i] = f256.multiplicationGF256(0b00000010,R[i-1]);
		return R;
	}
	
	public int[][] KeyExpansion(int[] Key, int Nb, int Nr, int Nk){
		int[] R = CreationRcon(Nb, Nr, Nk);
		int[][] W = new int[Nb*(Nr+1)][4];
		int[] temp;
		for(int i = 0; i < Nk; i++){
			for(int j = 0; j < 4; j++)
				W[i][j] = Key[4*i + j];
		}

		if(Nk <= 6){
			for(int i = Nk; i < Nb*(Nr+1); i++){
				temp = W[i-1];
				if(i % Nk == 0){
					temp = SubByte(RotByte(temp));
					temp[0] ^= R[i/Nk];
				}
				for(int j = 0; j < 4; j++)
					W[i][j] = W[i-Nk][j] ^ temp[j];
			}
		}else{
			for(int i = Nk; i < Nb*(Nr+1); i++){
				temp = W[i-1];
				if(i % Nk == 0){
					temp = SubByte(RotByte(temp));
					temp[0] ^= R[i/Nk];
				}
				else if(i % Nk == 4){
					temp = SubByte(temp);
				}
				for(int j = 0; j < 4; j++)
					W[i][j] = W[i-Nk][j] ^ temp[j];
			}
		}
		
		return W;
	}
	
	public int[][][] CreationRoundKeys(int[][] W, int nr, int nb){
		int[][][] RK = new int[nr+1][4][nb];
		for(int i = 0; i < nr+1; i++){
			for(int j = 0; j < nb; j++){
				for(int k = 0; k < 4; k++)
					RK[i][k][j] = W[i*nb+j][k];
			}
		}
		return RK;
	}
	
	
	public static void main(String args[]){
		String s = "Stefano345";
		KeySchedule testS = new KeySchedule(s, 192, 4, 15);
		System.out.println(testS);
	}
}