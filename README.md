This Java folder is for my projects in Java.

Currently they are two project available.

- **Fractal Project:** it is a concluded project. The program creates fractals in relation of the number of iterations and functions used to create the picture. Furthermore it is possible to download the picture obtained in a png file. To activate the program it is enough to download and use the file AESSte.jar from the repository or from the following link: http://www.yeyedo.com/JavaFiles/AESSte.jar

- **AES Project:** i it is a concluded project. This program permits to encrypt and decrypt in AES-128, AES-192 and AES-256. You can use it in a comfortable UI in Java. Be careful to not use it in illegal or improper way. To activate the program it is enough to download and use the file FracSte.jar from the repository or from the following link: http://www.yeyedo.com/JavaFiles/FracSte.jar

Both the projects are managed by Stefano Martin